var App = function () {

  var $this = this;

  $this.init = function () {
    $this.tabulator = createTabulator();
    // TODO: Другие инициализации...
  };

  var createTabulator = function () {
    return new Tabulator('#tabulator-table', {
      // height: 400,          // TODO: Статическая высотка таблицы увеличивает её производительность.
      data: [],                // Данные для первоначального отображения.
      layout: 'fitColumns',    // Раскладка колонок (растягиваются по ширине).
      pagination: 'remote',    // Включение удаленной пагинации.
      ajaxSorting: true,       // Включение удаленной сортировки.
      ajaxFiltering: true,     // Включение удаленной фильтрации.
      ajaxURL: '/browse',      // URL адрес удаленной пагинации.
      ajaxContentType: 'json', // Формат запроса для удаленной пагинации.
      ajaxConfig: 'POST',      // Метод запроса для удаленной пагинации.
      paginationSize: 10,      // Количество строк на одну страницу.
      paginationSizeSelector: [5, 10, 20, 50],

      columns: [
        {title: 'ID', field: 'id', width: 80, headerFilter: true, headerSortTristate: true},
        {title: 'Name', field: 'name', align: 'left', headerFilter: true, headerSortTristate: true},
        {title: 'Value', field: 'value', align: 'right', width: 120, headerFilter: true, headerSortTristate: true}
      ]
    });
  }
};

window.onload = function () {
  new App().init();
};
create sequence hibernate_sequence;

create table custom_data
(
    id bigint not null
        constraint custom_data_pkey
            primary key,
    name varchar(255),
    value numeric(19,2)
);
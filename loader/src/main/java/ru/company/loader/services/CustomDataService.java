package ru.company.loader.services;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.company.loader.entitities.CustomData;
import ru.company.loader.propeties.LoaderProperties;
import ru.company.loader.repositories.CustomDataRepository;
import ru.company.loader.repositories.specifications.CustomDataSpecification;
import ru.company.loader.vendors.TabulatorParams;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static ru.company.loader.repositories.specifications.CustomDataSpecification.identifierIs;
import static ru.company.loader.repositories.specifications.CustomDataSpecification.nameContains;
import static ru.company.loader.repositories.specifications.CustomDataSpecification.valueIs;

@Service
@Transactional
public class CustomDataService
{
    private Logger logger = LoggerFactory.getLogger(CustomDataService.class);

    private final LoaderProperties loaderProperties;
    private final CustomDataRepository customDataRepository;

    @Autowired
    public CustomDataService(LoaderProperties loaderProperties, CustomDataRepository customDataRepository)
    {
        this.loaderProperties = loaderProperties;
        this.customDataRepository = customDataRepository;
    }

    private volatile boolean loading = false;

    public boolean isLoading()
    {
        return loading;
    }

    /**
     * Обработка CSV файлов и сохранение информации из них в базу данных.
     * TODO: Многопоточность метода не реализована, запрещены повторные вызовы.
     */
    public void process()
    {
        if (this.loading) { return; }

        synchronized (CustomDataService.class) {
            if (this.loading) { return; }
            this.loading = true;
        }

        try {
            // Получение списка CSV файлов для обработки, включая файлы из вложенных директорий.
            final List<String> filenames = this.getFilenames(new File(this.loaderProperties.getIncomingPath()));

            for (final String filename : filenames) {
                try {
                    CSVReader reader = new CSVReaderBuilder(new FileReader(filename))
                            .withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_SEPARATORS)
                            .withCSVParser(new CSVParserBuilder().withSeparator(',').withQuoteChar('"').build())
                            .withSkipLines(1).build();

                    logger.info("Начата обработка файла: {}", filename);

                    List<CustomData> customDataList = new ArrayList<>();

                    Long currentPackageSize = 0L; // Текущий размер пакета из строк.
                    Long processedLinesCount = 0L; // Количество обработанных строк.

                    String[] nextLine;

                    while ((nextLine = reader.readNext()) != null) {

                        currentPackageSize++;
                        processedLinesCount++;

                        CustomData customData = new CustomData();

                        try {
                            customData.setId(Long.parseLong(nextLine[0]));
                            customData.setName(nextLine[1]);
                            customData.setValue(new BigDecimal(nextLine[2]));
                        } catch (Exception ex) {
                            logger.debug("Не удалось обработать строку: {}", processedLinesCount);
                            continue;
                        }

                        customDataList.add(customData);

                        // Загрузка строк, соответствующих максимальному пакету.
                        if (currentPackageSize.equals(this.loaderProperties.getMaxPackageSize())) {
                            customDataRepository.saveAll(customDataList);
                            logger.debug("Загружено строк в базу данных: {} ...", processedLinesCount);
                            customDataList.clear();
                            currentPackageSize = 0L;
                        }
                    }

                    // Загрузка оставшихся строк, некратных максимальному пакету.
                    if (customDataList.size() > 0) {
                        customDataRepository.saveAll(customDataList);
                    }

                    logger.debug("Всего загружено строк в базу данных: {}", processedLinesCount);
                    logger.info("Завершена обработка файла: {}", filename);

                    reader.close();

                    this.moveFile(filename, this.loaderProperties.getProcessedPath());

                } catch (FileNotFoundException ex) {
                    logger.error("Файл не найден: {}", filename);
                } catch (IOException ex) {
                    logger.error("Не удалось открыть файл: {}", filename);
                }
            }
        } finally {
            this.loading = false;
        }
    }

    /**
     * Постраничная загрузка данных.
     */
    public Page<CustomData> browse(TabulatorParams params)
    {
        // Сборка параметров сортировки.

        Sort sorting = Sort.unsorted();

        for (TabulatorParams.Sorter sorter : params.getSorters()) {
            if ("asc".equals(sorter.getDirection())) {
                sorting = sorting.and(Sort.by(sorter.getField()).ascending());
            } else if ("desc".equals(sorter.getDirection())) {
                sorting = sorting.and(Sort.by(sorter.getField()).descending());
            }
        }

        // Сборка параметров фильтрации.

        Specification<CustomData> specification = Specification.where(null);

        for (TabulatorParams.Filter filter : params.getFilters()) {
            if (CustomDataSpecification.ID.equals(filter.getField())) {
                specification = specification.and(identifierIs(Long.parseLong(filter.getValue())));
            } else if (CustomDataSpecification.NAME.equals(filter.getField())) {
                specification = specification.and(nameContains(filter.getValue()));
            } else if (CustomDataSpecification.VALUE.equals(filter.getField())) {
                specification = specification.and(valueIs(new BigDecimal(filter.getValue())));
            }
        }

        // Сборка параметров пагинации.

        final int page = params.getPage() - 1; // В Spring страницы начинаются с нулевой.
        final PageRequest pageRequest = PageRequest.of(page, params.getSize(), sorting);

        return customDataRepository.findAll(specification, pageRequest);
    }

    /**
     * Очистка всех данных.
     */
    public void delete()
    {
        customDataRepository.deleteAll();
        logger.info("Все загруженные данные удалены.");
    }

    /**
     * Сборка списка CSV файлов для обработки, включая вложенные директории.
     */
    private List<String> getFilenames(final File folder)
    {
        List<String> filenames = new LinkedList<>();
        final File[] files = folder.listFiles();

        if (files != null)
            for (final File file : files) {
                if (file.isDirectory()) {
                    filenames.addAll(getFilenames(file));
                } else if (file.getName().contains(".csv")) {
                    filenames.add(file.getAbsolutePath());
                }
            }

        logger.debug("Список файлов к обработке: {}", filenames);

        return filenames;
    }

    /**
     * Перемещение обработанных CSV файлов в соответствующую директорию.
     */
    private void moveFile(String filename, String targetPath)
    {
        final File file = new File(filename);

        if (targetPath.lastIndexOf('\\') != targetPath.length() - 1) {
            targetPath += "\\"; // Если не проставлен разделитель.
        }

        if (file.renameTo(new File(targetPath + file.getName()))) {
            logger.info("Файл перемещен: {}{}", targetPath, file.getName());
        } else {
            logger.error("Не удалось переместить файл: {}", file.getAbsolutePath());
        }
    }

    /**
     * Перемещение обработанных CSV файлов обратно во входящую директорию.
     */
    public void revert()
    {
        final File[] files = new File(this.loaderProperties.getProcessedPath()).listFiles();

        if (files != null) {
            for (File file : files) {
                moveFile(file.getAbsolutePath(), this.loaderProperties.getIncomingPath());
            }
        }
    }
}
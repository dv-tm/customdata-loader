package ru.company.loader.repositories.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.company.loader.entitities.CustomData;

import java.math.BigDecimal;

public class CustomDataSpecification
{

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String VALUE = "value";

    public static Specification<CustomData> identifierIs(Long id)
    {
        return (root, query, cb) -> id == null ? null : cb.equal(root.get(ID), id);
    }

    public static Specification<CustomData> nameContains(String name)
    {
        return (root, query, cb) -> name == null ? null : cb.like(root.get(NAME), "%" + name + "%");
    }

    public static Specification<CustomData> valueIs(BigDecimal value)
    {
        return (root, query, cb) -> value == null ? null : cb.equal(root.get(VALUE), value);
    }
}
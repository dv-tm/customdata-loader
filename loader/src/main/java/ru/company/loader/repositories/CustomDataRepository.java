package ru.company.loader.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.company.loader.entitities.CustomData;

import java.math.BigDecimal;

public interface CustomDataRepository extends PagingAndSortingRepository<CustomData, Long>, JpaSpecificationExecutor<CustomData>
{
    Page<CustomData> findByNameContains(String name, Pageable pageable);

    @Query(value = "select c from CustomData c where c.id = ?1 and c.name like %?2% and c.value = ?3")
    Page<CustomData> findByFilters(Long id, String name, BigDecimal value, Pageable pageable);

    Page<CustomData> findAll(Specification<CustomData> specification, Pageable pageable);
}
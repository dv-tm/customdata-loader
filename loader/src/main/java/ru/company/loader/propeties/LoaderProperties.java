package ru.company.loader.propeties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Configuration
@ConfigurationProperties(prefix = "app.loader")
@Validated
public class LoaderProperties
{
    @NotBlank(message = "В конфигурационном файле неверно задан параметр incomingPath.")
    private String incomingPath;

    @NotBlank(message = "В конфигурационном файле неверно задан параметр processedPath.")
    private String processedPath;

    @Value("${maxPackageSize:100}")
    private Long maxPackageSize;

    public String getIncomingPath()
    {
        return incomingPath;
    }

    public void setIncomingPath(String incomingPath)
    {
        this.incomingPath = incomingPath;
    }

    public String getProcessedPath()
    {
        return processedPath;
    }

    public void setProcessedPath(String processedPath)
    {
        this.processedPath = processedPath;
    }

    public Long getMaxPackageSize()
    {
        return maxPackageSize;
    }

    public void setMaxPackageSize(Long maxPackageSize)
    {
        this.maxPackageSize = maxPackageSize;
    }
}
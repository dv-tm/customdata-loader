package ru.company.loader.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController
{
    @RequestMapping(value = {"/", "/home", "/index"})
    private String home()
    {
        return "templates/home.html";
    }

    @RequestMapping("/about")
    private String about()
    {
        return "templates/about.html";
    }
}
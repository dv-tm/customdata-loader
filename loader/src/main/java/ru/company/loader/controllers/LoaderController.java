package ru.company.loader.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.company.loader.services.CustomDataService;

@Controller
public class LoaderController
{
    private final CustomDataService customDataService;

    public LoaderController(CustomDataService customDataService)
    {
        this.customDataService = customDataService;
    }

    @RequestMapping("/process")
    private String process()
    {
        if (customDataService.isLoading()) {
            return "templates/loading.html";
        }

        customDataService.process();

        return "templates/process.html";
    }

    @RequestMapping("/revert")
    private String revert()
    {
        customDataService.revert();
        return "templates/revert.html";
    }

    @RequestMapping("/delete")
    private String delete()
    {
        customDataService.delete();
        return "templates/delete.html";
    }
}
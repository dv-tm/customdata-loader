package ru.company.loader.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorController implements org.springframework.boot.web.servlet.error.ErrorController
{
    @RequestMapping("/error")
    private String error()
    {
        return "templates/error.html";
    }

    @Override
    public String getErrorPath()
    {
        return "templates/error.html";
    }
}
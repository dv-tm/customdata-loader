package ru.company.loader.controllers;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.company.loader.entitities.CustomData;
import ru.company.loader.services.CustomDataService;
import ru.company.loader.vendors.TabulatorParams;

import java.util.HashMap;
import java.util.Map;

@Controller
public class PageController
{
    private final CustomDataService customDataService;

    public PageController(CustomDataService customDataService)
    {
        this.customDataService = customDataService;
    }

    @RequestMapping("/pagination")
    private String pagination()
    {
        return "templates/pagination.html";
    }

    @ResponseBody
    @RequestMapping(value = "/browse")
    private Map<String, Object> browse(@RequestBody TabulatorParams params)
    {
        Map<String, Object> response = new HashMap<>();

        Page<CustomData> page = customDataService.browse(params);

        response.put("data", page.getContent());
        response.put("last_page", page.getTotalPages());

        return response;
    }
}
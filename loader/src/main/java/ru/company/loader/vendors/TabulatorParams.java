package ru.company.loader.vendors;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TabulatorParams
{
    public static class Sorter
    {
        private String field;
        private String direction;

        public String getField()
        {
            return field;
        }

        public void setField(String field)
        {
            this.field = field;
        }

        @JsonProperty("dir")
        public String getDirection()
        {
            return direction;
        }

        public void setDirection(String direction)
        {
            this.direction = direction;
        }
    }

    public static class Filter
    {
        private String field;
        private String type;
        private String value;

        public String getField()
        {
            return field;
        }

        public void setField(String field)
        {
            this.field = field;
        }

        public String getType()
        {
            return type;
        }

        public void setType(String type)
        {
            this.type = type;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }
    }

    private Integer page;
    private Integer size;
    private List<Sorter> sorters;
    private List<Filter> filters;

    public Integer getPage()
    {
        return page;
    }

    public void setPage(Integer page)
    {
        this.page = page;
    }

    public Integer getSize()
    {
        return size;
    }

    public void setSize(Integer size)
    {
        this.size = size;
    }

    public List<Sorter> getSorters()
    {
        return sorters;
    }

    public void setSorters(List<Sorter> sorters)
    {
        this.sorters = sorters;
    }

    public List<Filter> getFilters()
    {
        return filters;
    }

    public void setFilters(List<Filter> filters)
    {
        this.filters = filters;
    }
}